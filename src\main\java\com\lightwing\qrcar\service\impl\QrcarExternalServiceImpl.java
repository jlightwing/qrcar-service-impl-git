package com.lightwing.qrcar.service.impl;

import com.lightwing.qrcar.da.api.model.Vehicle;
import com.lightwing.qrcar.da.api.service.VehicleDA;
import com.lightwing.qrcar.jaxb.VehicleRequest;
import com.lightwing.qrcar.jaxb.VehicleResponse;
import com.lightwing.qrcar.service.api.QrcarExternalService;
import com.lightwing.qrcar.service.api.mapper.VehicleModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @see QrcarExternalService
 */
public class QrcarExternalServiceImpl implements QrcarExternalService
{
  @Autowired
  private VehicleDA vehicleDA;

  @Autowired
  private VehicleModelMapper vehicleModelMapper;

  /**
   * @see QrcarExternalService getByCode
   */
  public VehicleResponse getVehicleByCode(VehicleRequest request)
  {
    Vehicle vehicle = vehicleDA.getByCode(request.getCode());
    VehicleResponse response = vehicleModelMapper.toExternal(vehicle);


    return response;
  }
}
