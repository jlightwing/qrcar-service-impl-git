package com.lightwing.qrcar.service.impl;

import com.lightwing.qrcar.da.api.model.Vehicle;
import com.lightwing.qrcar.da.api.service.VehicleDA;
import com.lightwing.qrcar.jaxb.VehicleRequest;
import com.lightwing.qrcar.jaxb.VehicleResponse;
import com.lightwing.qrcar.service.api.mapper.VehicleModelMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class QrCarExternalServiceImplTest {

  @Mock
  VehicleModelMapper vehicleModelMapper;

  @Mock
  VehicleDA vehicleDA;

  @InjectMocks
  QrcarExternalServiceImpl qrcarExternalService;

  private Fixture fixture;

  public static final BigDecimal PRICE = BigDecimal.ONE;


  @Before
  public void setUp()
  {
    fixture = new Fixture();
  }


  @Test
  public void testGetVehicleByCodeSuccess()
  {
    fixture.givenThereIsAnExternalVehicleResponseToReturn();
    fixture.givenWeHaveAnInternalVehicleModel();
    fixture.givenWeHaveAVehicleRequest();
    fixture.givenVehicleDAGetByCodeReturnsVehicle();
    fixture.givenVehicleModelMapperReturnsExternalVehicleResponse();
    fixture.whenWeCallGetVehicleByCode();
    fixture.thenWeAssertTheVehicleByCodeResponse();
  }

  private class Fixture
  {
    private Vehicle vehicleInternal;

    private VehicleRequest vehicleRequest;
    private VehicleResponse vehicleResponse;
    private VehicleResponse vehicleByCodeResponse;

    public static final String CODE = "code";
    public static final String MAKE = "make";
    public static final String MODEL = "model";
    public static final String DESC = "desc";


    private void givenWeHaveAVehicleRequest()
    {
      vehicleRequest = new VehicleRequest();
      vehicleRequest.setCode(CODE);
    }

    private void givenWeHaveAnInternalVehicleModel()
    {
      vehicleInternal = new Vehicle();
      vehicleInternal.setCode(CODE);
      vehicleInternal.setMake(MAKE);
      vehicleInternal.setModel(MODEL);
      vehicleInternal.setDesc(DESC);
      vehicleInternal.setPrice(PRICE);
    }

    private void givenThereIsAnExternalVehicleResponseToReturn()
    {
      vehicleResponse = new VehicleResponse();

      vehicleResponse.setCode(CODE);
      vehicleResponse.setDesc(DESC);
      vehicleResponse.setMake(MAKE);
      vehicleResponse.setModel(MODEL);
      vehicleResponse.setPrice(PRICE);
    }

    private void givenVehicleDAGetByCodeReturnsVehicle()
    {
      when(vehicleDA.getByCode(CODE)).thenReturn(vehicleInternal);
    }

    private void givenVehicleModelMapperReturnsExternalVehicleResponse()
    {
      when(vehicleModelMapper.toExternal(vehicleInternal)).thenReturn(vehicleResponse);
    }

    private void whenWeCallGetVehicleByCode()
    {
      vehicleByCodeResponse = qrcarExternalService.getVehicleByCode(vehicleRequest);
    }

    private void thenWeAssertTheVehicleByCodeResponse()
    {
      assertEquals(CODE, vehicleByCodeResponse.getCode());
      assertEquals(DESC, vehicleByCodeResponse.getDesc());
      assertEquals(MAKE, vehicleByCodeResponse.getMake());
      assertEquals(MODEL, vehicleByCodeResponse.getModel());
      assertEquals(PRICE, vehicleByCodeResponse.getPrice());
    }
  }
}


